﻿Shader "Custom/CompileShader"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "black" {}
		_SecondTex("Second (RGB)", 2D) = "black" {}
		_ThirdTex("Second (RGB)", 2D) = "black" {}
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
		sampler2D _SecondTex;
		sampler2D _ThirdTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
			fixed4 b = fixed4 (0, 0, 0, 0);
            fixed4 f = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 s = tex2D (_SecondTex, IN.uv_MainTex);
			fixed4 t = tex2D (_ThirdTex, IN.uv_MainTex);
			b += f * f.a;
			b += s * s.a;
			b += t * t.a;
            o.Albedo = b.rgb;
			o.Alpha = 1;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
