﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class World
{
    public const string RGB_LAYER = "RGB";
    public const string TEST_LAYER = "TEST";
    public const string DEBUG_LAYER = "DEBUG";

    private static Dictionary<string, WorldLayer> _layers;
    public static Dictionary<string, WorldLayer> Layers
    {
        get
        {
            if (_layers == null)
            {
                _layers = new Dictionary<string, WorldLayer>();
            }

            return _layers;
        }
    }

    public static int Width;
    public static int Height;

    public static WorldLayer RGBWorld { get { return Layers[RGB_LAYER]; } }
    public static WorldLayer TestWorld { get { return Layers[TEST_LAYER]; } }
    public static WorldLayer DebugWorld { get { return Layers[DEBUG_LAYER]; } }


    public static void Init()
    {
        if (Layers.Count == 0)
        {
            Debug.LogError("Layers not initialized");
            return;
        }
        Width = Layers[RGB_LAYER].Texture.width;
        Height = Layers[RGB_LAYER].Texture.height;
    }

}
