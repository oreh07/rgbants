﻿using UnityEngine;
using System.Collections;

public class DebugLayer : WorldLayer
{
    public DebugLayer(RenderTexture renderTexture) : base(renderTexture)
    {
        IsActive = false;
    }

    public override void EveryFrame()
    {
        base.EveryFrame();

        if(IsActive != Input.GetKey(KeyCode.Space))
        {
            Fill(Color.clear);
            IsActive = Input.GetKey(KeyCode.Space);
        }
    }
}
