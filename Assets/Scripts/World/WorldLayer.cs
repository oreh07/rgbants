﻿using UnityEngine;
using System.Collections;

public class WorldLayer
{
    public Texture2D Texture { get; private set; }
    public RenderTexture RenderTexture { get; private set; }
    private int _textureWidth;
    private Color[] _colorArray;

    public bool IsActive { get; protected set; }

    public WorldLayer(RenderTexture renderTexture)
    {
        IsActive = true;
        RenderTexture = renderTexture;
        Texture = new Texture2D(renderTexture.width, renderTexture.height);
        _textureWidth = Texture.width;

        _colorArray = Texture.GetPixels();
        Fill(Color.clear);
    }

    public Color GetPixel(Vector2D v)
    {
        return GetPixel(v.x, v.y);
    }
    public Color GetPixel(int x, int y)
    {
        return _colorArray[x * _textureWidth + y];
    }

    public void SetPixels(Color[] colors)
    {
        if (!IsActive) return;
        Texture.SetPixels(colors);
    }
    public void SetPixel(int x, int y, Color color)
    {
        try
        {
            if (!IsActive) return;
            _colorArray[x * _textureWidth + y] = color;
        }
        catch (System.Exception ex)
        {
            Debug.LogErrorFormat("{0} {1} {2}", x, y, ex.Message);
        }
    }
    public void SetPixel(Vector2D v, Color color)
    {
        if (!IsActive) return;
        SetPixel(v.x, v.y, color);
    }

    public void Fill(Color color)
    {
        if (!IsActive) return;
        for (var i = 0; i < _colorArray.Length; ++i)
        {
            _colorArray[i] = color;
        }
        SetPixels(_colorArray);
        ApplyColors();
    }

    public virtual void EveryFrame()
    {
    }

    public virtual void ApplyColors()
    {
        if (!IsActive) return;
        SetPixels(_colorArray);
        Texture.Apply();
    }

    public void AddColor(Color col)
    {
        if (!IsActive) return;
        for (var i = 0; i < _colorArray.Length; ++i)
        {
            Color c = _colorArray[i];
            c.r += col.r;
            c.g += col.g;
            c.b += col.b;
            c.a += col.a;
            _colorArray[i] = c;
        }
        SetPixels(_colorArray);
    }

    public void FillPerlin()
    {
        float perlinScale = 0.9f;
        for (var x = 0; x < _textureWidth; x++)
        {
            for (var y = 0; y < _textureWidth; y++)
            {
                _colorArray[x * _textureWidth + y] = Mathf.PerlinNoise(((float)x) / _textureWidth * perlinScale, ((float)y) / _textureWidth * perlinScale) * Color.white;
            }
        }
        SetPixels(_colorArray);

    }
}
