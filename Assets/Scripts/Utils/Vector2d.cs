﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector2DExtensions
{
    public static Vector2D Add(this Vector2D c1, Vector2D c2)
    {
        c1.x = ((c1.x + c2.x) % World.Width);
        if (c1.x < 0) c1.x += World.Width;
        c1.y = ((c1.y + c2.y) % World.Height);
        if (c1.y < 0) c1.y += World.Height;
        return c1;
    }

    public static Vector2D Sub(this Vector2D c1, Vector2D c2)
    {
        c1.x = ((c1.x - c2.x) % World.Width);
        if (c1.x < 0) c1.x += World.Width;
        c1.y = ((c1.y - c2.y) % World.Height);
        if (c1.y < 0) c1.y += World.Height;
        return c1;
    }
}


public class Vector2D
{

    public int x;
    public int y;

    public Vector2D(int _x = 0, int _y = 0)
    {
        x = _x;
        y = _y;
    }

    public static Vector2D operator +(Vector2D c1, Vector2D c2)
    {
        Vector2D r = new Vector2D((c1.x + c2.x) % World.Width, (c1.y + c2.y) % World.Height);
        if (r.x < 0) r.x += World.Width;
        if (r.y < 0) r.y += World.Height;
        return r;
    }

    public static Vector2D operator -(Vector2D c1, Vector2D c2)
    {
        Vector2D r = new Vector2D((c1.x - c2.x) % World.Width, (c1.y - c2.y) % World.Height);
        if (r.x < 0) r.x += World.Width;
        if (r.y < 0) r.y += World.Height;
        return r;
    }

    public override string ToString()
    {
        return string.Format("({0}, {1})", x, y);
    }
}

