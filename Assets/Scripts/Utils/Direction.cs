﻿using UnityEngine;
using System.Collections;


//   ABC
//   789
//   4w6  w - self
//   123
public enum EDirection
{
    dA = 9,  dB = 10, dC = 11,
    d7 = 5,  d8 = 1,  d9 = 6,
    d4 = 4,  dw = 0,  d6 = 2,
    d1 = 8,  d2 = 3,  d3 = 7,
}

public static class Direction
{
    static readonly Vector2D vectorD1 = new Vector2D(-1, -1);
    static readonly Vector2D vectorD2 = new Vector2D( 0, -1);
    static readonly Vector2D vectorD3 = new Vector2D( 1, -1);

    static readonly Vector2D vectorD4 = new Vector2D(-1,  0);
    static readonly Vector2D vectorDw = new Vector2D( 0,  0);
    static readonly Vector2D vectorD6 = new Vector2D( 1,  0);

    static readonly Vector2D vectorD7 = new Vector2D(-1,  1);
    static readonly Vector2D vectorD8 = new Vector2D( 0,  1);
    static readonly Vector2D vectorD9 = new Vector2D( 1,  1);

    static readonly Vector2D vectorDA = new Vector2D(-1,  2);
    static readonly Vector2D vectorDB = new Vector2D( 0,  2);
    static readonly Vector2D vectorDC = new Vector2D( 1,  2);

    public static EDirection[] Directions4 = new EDirection[4] { EDirection.d2, EDirection.d4, EDirection.d6, EDirection.d8 };
    public static EDirection[] Directions8 = new EDirection[8] { EDirection.d1, EDirection.d2, EDirection.d3, EDirection.d4, EDirection.d6, EDirection.d7, EDirection.d8, EDirection.d9 };

    public static EDirection GetRandom8()
    {
        return (EDirection)Random.Range(1, 9);
    }
    public static EDirection GetRandom4()
    {
        return (EDirection)Random.Range(1, 5);
    }
    public static bool IsNear(EDirection dir)
    {
        return (int)dir < (int)EDirection.dA;
    }
    public static EDirection TowardsTo(EDirection to)
    {
        switch (to)
        {
            case EDirection.dA: return EDirection.d7;
            case EDirection.dB: return EDirection.d8;
            case EDirection.dC: return EDirection.d9;
            default: return to;
        }
    }

    public static Vector2D Convert(EDirection direction)
    {
        switch (direction)
        {
            case EDirection.d1: return vectorD1;
            case EDirection.d2: return vectorD2;
            case EDirection.d3: return vectorD3;

            case EDirection.d4: return vectorD4;
            case EDirection.dw: return vectorDw;
            case EDirection.d6: return vectorD6;

            case EDirection.d7: return vectorD7;
            case EDirection.d8: return vectorD8;
            case EDirection.d9: return vectorD9;

            //case EDirection.dA: return new Vector2D(-1, -2);
            //case EDirection.dB: return new Vector2D(0,  -2);
            //case EDirection.dC: return new Vector2D(1,  -2);
            default: return new Vector2D();
        }
    }
    // возвращает позицию от (-1, -1) до (1, 1)
    // нужно направление юнита и направление движение
    public static Vector2D DirectionToVector(EDirection unitDirection, EDirection moveTo)
    {
        moveTo = TowardsTo(moveTo);
        return Convert(moveTo);
    }
}
