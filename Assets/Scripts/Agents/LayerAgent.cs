﻿using System.Collections;
using System.Collections.Generic;
using Creatures;
using UnityEngine;

namespace Creatures
{
    public class LayerAgent : Agent
    {
        private WorldLayer world;
        private Color color;

        public LayerAgent(WorldLayer world, Vector2D pos, Color col)
        {
            this.world = world;
            Position = pos;
            color = col;
        }

        public override void Action() { }

        public override void Render()
        {
            const int d = 100;
            Color c;

            for (int i = -d; i < d; i++)
            {
                for (int j = -d; j < d; j++)
                {
                    float intense = 0.4f * (float)d / (Mathf.Abs(i) + Mathf.Abs(j)) + Mathf.Epsilon;

                    Vector2D current = new Vector2D(Position.x, Position.y);
                    current.x = (current.x + i + World.Width) % World.Width;
                    current.y = (current.y + j + World.Height) % World.Height;

                    c = world.GetPixel(current);
                    c.r = color.r * intense;
                    c.a = (c.r + c.g + c.b) / 1f;
                    world.SetPixel(current, c);
                }
            }
        }
    }
}