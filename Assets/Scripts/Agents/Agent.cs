﻿
namespace Creatures
{
    public abstract class Agent
    {
        protected Vector2D Position;

        public abstract void Action();
        public abstract void Render();
    }
}