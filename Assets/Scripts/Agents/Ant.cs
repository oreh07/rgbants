﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Creatures
{
    public class Ant : Agent
    {
        //EDirection direction;

        //bool food;
        //bool materials;
        //bool searching;
        //bool danger;
        //bool cargo;
        //int feromon;

        public Ant()
        {
            Position = new Vector2D(
                Random.Range(0, World.Width), 
                Random.Range(0, World.Height)
                );
            //direction = EDirection.d8;
            //food = false;
            //materials = false;
            //searching = false;
            //danger = false;
            //cargo = false;
            //feromon = 0;
        }

        public override void Action()
        {
            Searching();
            CheckArea();
        }

        protected void CheckArea()
        {
        }

        protected void Searching()
        {
            float value = float.MaxValue;
            EDirection min = EDirection.d8;
            //IEnumerable directions = Direction.GetNearDirections();
            //foreach (EDirection dir in directions)
            //{
            //    float v = World.rgb.GetPixel(position + Direction.Convert(dir)).r;
            //    if (value > v)
            //    {
            //        min = dir;
            //        value = v;
            //    }
            //}

            _lastPosition = Position;
            Position.Add(Direction.Convert(min));
            //Debug.Log(position);
        }

        private Vector2D _lastPosition;
        private Color _under;
        public override void Render()
        {
            WorldLayer wl = World.RGBWorld;
            if (_under != null)
            {
                wl.SetPixel(_lastPosition, _under);
            }
            _under = wl.GetPixel(Position);
            wl.SetPixel(Position, Color.red);

            wl = World.TestWorld;
            if (_under != null)
            {
                wl.SetPixel(_lastPosition, _under);
            }
            _under = wl.GetPixel(Position);
            wl.SetPixel(Position, Color.green);

            wl = World.DebugWorld;
            if (_under != null)
            {
                wl.SetPixel(_lastPosition, _under);
            }
            _under = wl.GetPixel(Position);
            wl.SetPixel(Position, Color.blue);
        }
    }
}
