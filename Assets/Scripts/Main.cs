﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Creatures;



[RequireComponent(typeof(Camera))]
public class Main : MonoBehaviour {
    [SerializeField] private Renderer _renderer;

    private RenderTexture[] _worlds;
    private Ant[] _ants;
    private bool _init = true;

    private void Start()
    {
        InitWorldLayers();
        InitAgents();
    }

    private void InitWorldLayers()
    {
        _worlds = new RenderTexture[3];
        _renderer.material = new Material(Shader.Find("Custom/CompileShader"));
        for (int i = 0; i < _worlds.Length; i++)
        {
            _worlds[i] = new RenderTexture(256, 256, 32);
        }

        World.Layers.Add(World.RGB_LAYER,   new WorldLayer(_worlds[0]));
        World.Layers.Add(World.TEST_LAYER,  new WorldLayer(_worlds[1]));
        World.Layers.Add(World.DEBUG_LAYER, new DebugLayer(_worlds[2]));

        _renderer.material.SetTexture("_MainTex",   World.Layers[World.RGB_LAYER].Texture);
        _renderer.material.SetTexture("_SecondTex", World.Layers[World.TEST_LAYER].Texture);
        _renderer.material.SetTexture("_ThirdTex",  World.Layers[World.DEBUG_LAYER].Texture);

        World.Init();
    }

    private void InitAgents()
    {
        int count = 10;
        _ants = new Ant[count];
        for (int i = 0; i < count; i++)
        {
            Ant ant = new Ant();
            _ants[i] = ant;
        }
    }

    private void OnPreRender()
    {
        if (!_init) return;

        RenderTexture.active = World.RGBWorld.RenderTexture;
        
        foreach (Ant ant in _ants)
        {
            ant.Action();
            ant.Render();
        }

        World.RGBWorld.EveryFrame();
        World.RGBWorld.AddColor(Color.white * -0.01f);
        World.RGBWorld.ApplyColors();

        World.TestWorld.EveryFrame();
        World.TestWorld.AddColor(Color.white * -0.01f);
        World.TestWorld.ApplyColors();

        World.DebugWorld.EveryFrame();
        World.DebugWorld.AddColor(Color.white * -0.01f);
        World.DebugWorld.ApplyColors();

        RenderTexture.active = null;

#if UNITY_EDITOR
        DrawLayerBorders(_renderer.transform);
#endif
    }


#if UNITY_EDITOR
    private void DrawLayerBorders(Transform tr)
    {
        Vector3 center = tr.position;
        Debug.DrawLine(new Vector3(center.x - 1, center.y, center.z - 1), new Vector3(center.x - 1, center.y, center.z + 1));
        Debug.DrawLine(new Vector3(center.x - 1, center.y, center.z + 1), new Vector3(center.x + 1, center.y, center.z + 1));
        Debug.DrawLine(new Vector3(center.x + 1, center.y, center.z + 1), new Vector3(center.x + 1, center.y, center.z - 1));
        Debug.DrawLine(new Vector3(center.x + 1, center.y, center.z - 1), new Vector3(center.x - 1, center.y, center.z - 1));
    }
#endif
}
